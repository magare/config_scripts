#!/bin/bash

sudo apt-get -y install git cmake python python3 python3-pip python-pip python-dev

sudo ln -s ${PWD}/ttmux /usr/bin/ttmux

git submodule init && git submodule update

pushd vim/vim/bundle/YouCompleteMe/
git submodule update --init --recursive
popd

bash vim/bin/init.sh
rm -rf ~/.tmux.conf ~/.vimrc
cp tmux.conf ~
cp vimrc ~
mv ~/tmux.conf ~/.tmux.conf
mv ~/vimrc ~/.vimrc

cat HashBang/Hashbang >> ~/.vimrc

