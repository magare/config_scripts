#! /usr/bin/env bash
sudo rmmod sdhci_pci
sudo rmmod sdhci_acpi || true
sudo rmmod sdhci || true
sudo modprobe sdhci debug_quirks2="0x10000"
sudo modprobe sdhci_pci
sudo modprobe sdhci_acpi
